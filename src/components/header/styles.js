import styled from "styled-components";


export const HeaderStyle = styled.div`
  display: flex;
  background: #3754E2;
  justify-content: center;
  align-items: center;
  height: 12vh;
  width:100vw;
  margin-top: -8px;
  margin-left: -8px;
  color:white;
  font-size: 1.5rem;
  text-align:center;
`;

          
export const Container = styled.div`
display:flex;
flex-direction:row;
justify-content:space-evenly;
align-items:center;
height:100vh;
width:100vw;
background-color: gray;
`;

export const InfoBox = styled.div`
  box-shadow: 0 2px 4px #D9D9D9;
  padding: $padding-base $padding-small;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  background-color: #fff;
  margin-bottom: $margin-base;
  width:10vw;
  max-height:18vh;
`;
          
export const BoxTitle = styled.div`
  font: $font-weight-black #{$font-size-wee}/1.375rem $font-family-primary;
  color: #666;
  opacity: 0.5;
  margin: $margin-wee 0 0 0;
  text-transform: uppercase;
`;         


export const BoxValue = styled.div`
font: 1.25rem/1.375rem $font-family-primary;
  color: #333;
`;


  /* @include media-breakpoint-up(lg) {
    font-size: $font-size-medium;
    margin-bottom: $margin-tiny; */




  /* @include media-breakpoint-up(lg) 
    font-size: 1.625rem;
    line-height: 1.8125rem; */




