import Routes from "./routes/index"
import { motion } from "framer-motion";

import Card from "./components/header/index"



function App() {

  document.title = "Choose your song"

return (
  <motion.div
  initial={{ opacity: 0 }}
  animate={{ opacity: 1 }}
  exit={{ opacity: 0 }}
  transition={{ duration: 2 }}
>
  <Routes />
</motion.div>
);
}

export default App;
