import { Container, StyledInput } from "../../styles"
import { useState } from "react"
import { motion } from "framer-motion"

function StayTheKidLaroi() {

  document.title = "The kid Laroi, Justin Bieber - Stay"

  const [myBank, setMyBank] = useState([]);
  const [score, setScore] = useState(0);



  const checkWords = (word) => {


    if( word === 'wasted'|| word === 'way' || word === 'believe' || word === 'afraid' || word === 'would' || word === 'could' || word === 'as' || word === 'to' || word === "can't" || word === 'do' || word === 'without' || word === 'trust' || word === 'empty' || word === "I'd" || word === 'realize') {
      alert('right word!');
      setMyBank([...myBank," ", word]); 
      setScore(score+1);

      return myBank
    }
  }
  

  return (
    <motion.div
  initial={{ opacity: 0 }}
  animate={{ opacity: 1 }}
  exit={{ opacity: 0 }}
  transition={{ duration: 2 }}
>
    <Container>
    <h1>The kid Laroi, Justin Bieber - Stay</h1>
    <br/>
    <br/>
    <span>(...)</span>
    <br/>
    <span>I get drunk, wake up, I'm <StyledInput onChange={(e) => checkWords(e.target.value)} /> still</span>
    <span>I <StyledInput onChange={(e) => checkWords(e.target.value)} /> the time that I wasted here</span>
    <span>I feel like you can't feel the <StyledInput onChange={(e) => checkWords(e.target.value)} /> I feel</span>
    <span>Oh, I'll be **** up if you can't be right here</span>
    <br/>
    <span>Oh~</span>
    <span>Oh, I'll be **** up if you can't be right here</span>
    <br/>
    <br/>
    <h4>Chorus</h4>
    <span>I <StyledInput onChange={(e) => checkWords(e.target.value)} /> the same thing I told you that I never <StyledInput onChange={(e) => checkWords(e.target.value)} /></span>
    <span>I told you <StyledInput onChange={(e) => checkWords(e.target.value)} /> change, even when I knew I never <StyledInput onChange={(e) => checkWords(e.target.value)} /></span>
    <span>I know that I <StyledInput onChange={(e) => checkWords(e.target.value)} /> find nobody else <StyledInput onChange={(e) => checkWords(e.target.value)} /> good as you</span>
    <span>I need you <StyledInput onChange={(e) => checkWords(e.target.value)} /> stay, need you to stay, hey</span>
    <br/>
    <br/>
    <span>When I'm away from you, I miss your touch (Ooh)</span>
    <span>You're the reason I <StyledInput onChange={(e) => checkWords(e.target.value)} /> in love</span>
    <span>It's been difficult for me to <StyledInput onChange={(e) => checkWords(e.target.value)} />  (Ooh)</span>
    <span>And I'm <StyledInput onChange={(e) => checkWords(e.target.value)} /> that I'ma fuck it up</span>
    <span>Ain't no way that I can leave you stranded</span>
    <span>'Cause you ain't never left me <StyledInput onChange={(e) => checkWords(e.target.value)} />-handed</span>
    <span>And you know that I know that I can't live <StyledInput onChange={(e) => checkWords(e.target.value)} />  you</span>
    <span>So, baby, stay</span>
    <br/>
    <h4>Chorus repeat</h4>





    <h5>This is my bank: {myBank}</h5>
    <h5>Total words I could figure out is: {score}</h5>
    {score === 9 && <div> Congratulations! You've achieved the maximum score!</div>}
  </Container>
  </motion.div>
  );
}

export default StayTheKidLaroi;
