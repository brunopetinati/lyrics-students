import { Container, StyledInput } from "../../styles"
import { useState } from "react"
import { motion } from "framer-motion"
 

function RiptideVanceJoy() {

  document.title = "Riptide - Vance Joy"

  const [myBank, setMyBank] = useState([]);
  const [score, setScore] = useState(0);



  const checkWords = (word) => {


    if( word === 'conversations'|| word === 'dream' || word === 'riptide' || word === 'taken' || word === 'wrong' || word === 'closest' || word === 'destined' || word === 'heads' || word === 'lump' ) {
      alert('right word!');
      setMyBank([...myBank," ", word]); 
      setScore(score+1);

      return myBank
    }
  }
  

  return (
    <motion.div
  initial={{ opacity: 0 }}
  animate={{ opacity: 1 }}
  exit={{ opacity: 0 }}
  transition={{ duration: 2 }}
>
    <Container>
    <h1>Riptide - Vance Joy</h1>
    <br/>
    <br/>
    <span>I was scared of dentists and the dark</span>
    <span>I was scared of pretty girls and starting <StyledInput onChange={(e) => checkWords(e.target.value)}></StyledInput></span>
    <span>Oh, all my friends are turning green</span>
    <span>You're the magician's assistant in their <StyledInput onChange={(e) => checkWords(e.target.value)}></StyledInput></span>
    <br/>
    <span>Oh, oh, and they come unstuck</span>
    <br/>

    <h4>Chorus</h4>
    <span>Lady, running down to the <StyledInput onChange={(e) => checkWords(e.target.value)}></StyledInput></span>
    <span><StyledInput onChange={(e) => checkWords(e.target.value)}></StyledInput> away to the dark side</span>
    <span>I wanna be your left-hand man</span>
    <span>I love you when you're singing that song and</span>
    <span>I got a  <StyledInput onChange={(e) => checkWords(e.target.value)}></StyledInput> in my throat 'cause</span>
    <span>You're gonna sing the words <StyledInput onChange={(e) => checkWords(e.target.value)}></StyledInput></span>
    <br/>
    <br />
    <span>Here's movie that I think you'll like</span>
    <span>This guy decides to quit his job and <StyledInput onChange={(e) => checkWords(e.target.value)}></StyledInput> to New York City</span>
    <span>This cowboy's running from himself</span>
    <span>And she's been living on the highest shelf</span>
    <br/>
    <span>Oh, oh, and they come unstuck</span>
    <br/>

    <h4>Chorus repeat</h4>

    <br/>
    <span>I just wanna, I just wanna know</span>
    <span>If you're gonna, if you're gonna stay</span>
    <span>I just gotta, I just gotta know</span>
    <span>I can't have it, I can't have it any other way</span>
    <span>I swear she's  <StyledInput onChange={(e) => checkWords(e.target.value)}></StyledInput> for the screen</span>
    <span> <StyledInput onChange={(e) => checkWords(e.target.value)}></StyledInput> thing to Michelle Pfeiffer that you've ever seen, oh</span>
    <br/>
    <h4>Chorus repeat 2x</h4>

    <h5>This is my bank: {myBank}</h5>
    <h5>My score is: {score}</h5>
    {score === 9 && <div> Congratulations! You've achieved the maximum score!</div>}
  </Container>
  </motion.div>
  );
}

export default RiptideVanceJoy;
