import { Container, StyledInput } from "../../styles"
import { useState } from "react"
import { motion } from "framer-motion";


function Renegades() {

  document.title = "Renegades - X Ambassadors"

  const [myBank, setMyBank] = useState([]);
  const [score, setScore] = useState(0);



  const checkWords = (word) => {


    if( word === 'wild'|| word === 'pioneers' || word === 'Rebels' || word === 'rebels' || word === 'fear' || word === 'lend' || word === 'underdogs' || word === 'outlaws' || word === 'amends' || word === 'say') {
      alert('right word!');
      setMyBank([...myBank," ", word]); 
      setScore(score+1);

      return myBank
    }
  }
  

  return (
<motion.div
  initial={{ opacity: 0 }}
  animate={{ opacity: 1 }}
  exit={{ opacity: 0 }}
  transition={{ duration: 2 }}
>
    <Container>
    <h1>Renegades - X Ambassadors</h1>
    <br/>
    <br/>

    <span>Run away-ay with me</span>
    <span>Lost souls in revelry</span>
    <span>Running  <StyledInput onChange={(e) => checkWords(e.target.value)} /> and running free</span>
    <span>Two kids, you and me</span>
    <br/>

    <h4>Chorus</h4>
    <span>And I <StyledInput onChange={(e) => checkWords(e.target.value)}/></span>
    <span>Hey, hey-hey-hey</span>
    <span>Livin' like we're renegades</span>
    <span>Hey-hey-hey</span>
    <span>Hey-hey-hey</span>
    <span>Livin' like we're renegades</span>
    <span>Renegades</span>
    <span>Renegades   </span>
    <br/>
    <br/>
    <span>Long live the  <StyledInput onChange={(e) => checkWords(e.target.value)}/></span>
    <span><StyledInput onChange={(e) => checkWords(e.target.value)}/> and mutineers</span>
    <span>Go forth and have no <StyledInput onChange={(e) => checkWords(e.target.value)}/></span>
    <span>Come close and <StyledInput onChange={(e) => checkWords(e.target.value)}/> an ear</span>
    <br/>
    <h4>Chorus repeat</h4>
   
    <br/>
    <span>All hail the <StyledInput onChange={(e) => checkWords(e.target.value)}/></span>
    <span>All hail the new kids</span>
    <span>All hail the <StyledInput onChange={(e) => checkWords(e.target.value)}/></span>
    <span>Spielbergs and Kubricks</span>
    <br/>
    <br/>
    <span>It's our time to make a move</span>
    <span>It's our time to make <StyledInput onChange={(e) => checkWords(e.target.value)}/></span>
    <span>It's our time to break the rules</span>
    <span>Let's begin</span>  

    <h4>Chorus repeat</h4>

    <br/>
    <br/>
    <h5>This is my bank: {myBank}</h5>
    <h5>My score is: {score}</h5>
    {score === 9 && <div> Congratulations! You've achieved the maximum score!</div>}
  </Container>
  </motion.div>
  );
}

export default Renegades;
