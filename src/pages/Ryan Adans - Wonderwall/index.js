import { Container, StyledInput, Space } from "../../styles"
import { useState } from "react"
import { motion } from "framer-motion"

function WonderWallRyanAdams() {

  document.title = "Ryan Adams - Wonderwall"

  const [myBank, setMyBank] = useState([]);
  const [score, setScore] = useState(0);



  const checkWords = (word) => {


    if( word === 'anybody'|| word === 'winding' || word === 'blinding' || word === 'would' || word === 'would' || word === 'there' || word === 'bring' || word === 'somehow' || word === "feels" || word === 'Feels' || word === 'do' || word === 'lead' || word === 'There' || word === "who" || word === 'saves' || word === 'all' || word === 'wonderwall' || word === 'how') {
      alert('right word!');
      setMyBank([...myBank," ", word]); 
      setScore(score+1);

      return myBank
    }
  }
  

  return (
    <motion.div
  initial={{ opacity: 0 }}
  animate={{ opacity: 1 }}
  exit={{ opacity: 0 }}
  transition={{ duration: 2 }}
>
    <Container>
    <h1>Ryan Adams - Wonderwall</h1>
    <Space />
    <span>Today is gonna be the day</span>
    <span>That they're gonna give it back to you</span>
    <span>By now you should've <StyledInput onChange={(e) => checkWords(e.target.value)} /></span>
    <span>Realized what you gotta do</span>
    <span>I don't believe that <StyledInput onChange={(e) => checkWords(e.target.value)} /></span>
    <span><StyledInput onChange={(e) => checkWords(e.target.value)} /> the way I <StyledInput onChange={(e) => checkWords(e.target.value)} /> about you now</span>
    <Space />
    <span>Back beat the word was on the street</span>
    <span>That the fire in your heart is out</span>
    <span>And I'm sure you've heard it all before</span>
    <span>But you never really had a doubt</span>
    <span>(...)</span>
    <Space />
    <span>And all the roads we have to walk are <StyledInput onChange={(e) => checkWords(e.target.value)} /></span>
    <span>And all the lights that <StyledInput onChange={(e) => checkWords(e.target.value)} /> the way are <StyledInput onChange={(e) => checkWords(e.target.value)} /></span>
    <span><StyledInput onChange={(e) => checkWords(e.target.value)} /> are many things that I <StyledInput onChange={(e) => checkWords(e.target.value)} /></span>
    <span>Like to say to you</span>
    <span>But I don't know <StyledInput onChange={(e) => checkWords(e.target.value)} /></span>
    <Space />
    <h4>Chorus</h4>
    <span>I said maybe</span>
    <span>You're gonna be the one that <StyledInput onChange={(e) => checkWords(e.target.value)} /> me?</span>
    <span>And after all</span>
    <span>You're my wonderwall</span>
    <Space />
    <span>Today was gonna be the day</span>
    <span>But they'll never <StyledInput onChange={(e) => checkWords(e.target.value)} /> it back to you</span>
    <span>By now you should've somehow</span>
    <span>Realized what you got to do</span>
    <span>I don't believe that anybody</span>
    <span>Feels the way I do</span>
    <span>About you now</span>
    <Space />
    <h4>Chorus repeat</h4>
    <Space />
    <span>I said maybe</span>
    <span>You're gonna be the one that <StyledInput onChange={(e) => checkWords(e.target.value)} /> me?</span>  
    <span>And after <StyledInput onChange={(e) => checkWords(e.target.value)} /></span>
    <span>You're my <StyledInput onChange={(e) => checkWords(e.target.value)} /></span>
    <Space />

    <h5>This is my bank: {myBank}</h5>
    <h5>Total words I could figure out is: {score}</h5>
    {score === 15 && <div> Congratulations! You've achieved the maximum score!</div>}
  </Container>
  </motion.div>
  );
}

export default WonderWallRyanAdams;
