import { Container, StyledInput, Space } from "../../styles"
import { useState } from "react"
import { motion } from "framer-motion"

function NextToMeRufusDuSol() {

  document.title = "Next to me - Rufus du Sol"

  const [myBank, setMyBank] = useState([]);
  const [score, setScore] = useState(0);



  const checkWords = (word) => {


    if( word === 'anybody'|| word === 'knew' || word === 'would' || word === 'since' || word === 'first' || word === 'you' || word === 'the'|| word === 'one' || word === 'tell'|| word === 'standing' || word === 'twilight'|| word === 'lights' || word === 'hear'||  word === 'time'|| word === 'front' || word === 'Tell' || word === 'You') {
      alert('right word!');
      setMyBank([...myBank," ", word]); 
      setScore(score+1);

      return myBank
    }
  }
  

  return (
    <motion.div
  initial={{ opacity: 0 }}
  animate={{ opacity: 1 }}
  exit={{ opacity: 0 }}
  transition={{ duration: 2 }}
>
    <Container>
    <h2>Rufus du Sol - Next to me </h2>

    <Space />

    <h4>Chorus</h4>

    <span>And when the <StyledInput onChange={(e) => checkWords(e.target.value)} /> come down</span>
    <span>I wanna feel you standing next to me</span>
    <span>And can you <StyledInput onChange={(e) => checkWords(e.target.value)} /> me out?</span>
    <span>There's a lifetime in <StyledInput onChange={(e) => checkWords(e.target.value)} /> of me</span>
    <span>And when my <StyledInput onChange={(e) => checkWords(e.target.value)} /> runs out</span>
    <span>I wanna feel you standing next to me</span>

    <span>Baby, I <StyledInput onChange={(e) => checkWords(e.target.value)} /> this time would arrive</span>
    <span>I knew this time <StyledInput onChange={(e) => checkWords(e.target.value)} /> come</span>
    <span>Ever <StyledInput onChange={(e) => checkWords(e.target.value)} /> I <StyledInput onChange={(e) => checkWords(e.target.value)} /> looked in your eyes</span>
    <span>I said, "<StyledInput onChange={(e) => checkWords(e.target.value)} />'re <StyledInput onChange={(e) => checkWords(e.target.value)} /> <StyledInput onChange={(e) => checkWords(e.target.value)} />"</span>
    <span>Oh my God, oh my God</span>
    <span><StyledInput onChange={(e) => checkWords(e.target.value)} /> me I'm the one</span>
    <span>Oh my God, oh my God</span>
    <Space />
    <span>I wanna feel you <StyledInput onChange={(e) => checkWords(e.target.value)} /> next to me</span>
    <Space />


    <span>Late in the evening when the sun comes down</span>
    <span>Under the starlight as the birds fly by</span>
    <span>I see the dust fill the <StyledInput onChange={(e) => checkWords(e.target.value)} /> skies</span>
    <span>I feel the warmth of your hand in mine</span>
    <Space />
    <span>It's in the air, it's in the air</span>
    <span>It's in the air we breathe</span>
    <span>It's all around, it's all around us now</span>
    <Space />
    <h4>Chorus repeat </h4>
    <Space />
    <span>And when the lights come down</span>
    <span>I wanna feel you standing next to me</span>


    <Space />
    <h5>This is my bank: {myBank}</h5>
    <h5>Total words I could figure out is: {score}</h5>
    {score === 14 && <div> Congratulations! You've achieved the maximum score!</div>}
  </Container>
  </motion.div>
  );
}

export default NextToMeRufusDuSol;












