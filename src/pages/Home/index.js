import { HomeContainer, Links } from "../../styles/index"
import { useHistory } from "react-router-dom"

const Home = () => {

const history = useHistory(); 

return (

<>

<HomeContainer>

<Links onClick={() => history.push("/renegades") }>Renegades - X Ambassadors</Links>
<Links onClick={() => history.push("/stay-the-kid-laroi") }>Stay - The Kid Laroi ft Justin Bieber</Links>
<Links onClick={() => history.push("/nocturnalnotions") }>Nocturnal Notions - Anthony Amorim</Links>
<Links onClick={() => history.push("/riptide") }>Riptide - Vance Joy</Links>
<Links onClick={() => history.push("/wonderwall") }>Wonderwall - Ryan Adams</Links>
<Links onClick={() => history.push("/nextome") }>Next to me - Rufus du Sol</Links>

</HomeContainer>
</>
)

}

export default Home;