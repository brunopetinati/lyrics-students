import { Container, StyledInput } from "../../styles"
import { useState } from "react"
import { motion } from "framer-motion"

const NocturnalNotions = () => {


document.title = "Nocturnal Notions - Anthony Amorim"

const [myBank, setMyBank] = useState([]);
const [score, setScore] = useState(0);



const checkWords = (word) => {


  if( word === 'an'|| word === 'battle' || word === 'stand' || word === 'there' || word === 'curse' || word === 'worse' || word === 'reaching' || word === 'to' || word === "bright" || word === 'lullaby' || word === 'source' ||  word === 'There' || word === 'why') {
    alert('right word!');
    setMyBank([...myBank," ", word]); 
    setScore(score+1);

    return myBank
  }
}


return (
  <motion.div
  initial={{ opacity: 0 }}
  animate={{ opacity: 1 }}
  exit={{ opacity: 0 }}
  transition={{ duration: 2 }}
>
  <Container>
  <h1>Nocturnal Notions - Anthony Amorim</h1>
  <br/>
  <br/>
  <span>When the day dies down and comes to <StyledInput onChange={(e) => checkWords(e.target.value)} />  end</span>
  <span>And you're left with just your thoughts, the <StyledInput onChange={(e) => checkWords(e.target.value)} /> of your dissent</span>
  <span>Every day brings on a new <StyledInput onChange={(e) => checkWords(e.target.value)} />  you must fight</span>
  <span>I'll <StyledInput onChange={(e) => checkWords(e.target.value)} />  next to you, I'll get you through the night</span>
  <br/>
  <br/>
  <span><StyledInput onChange={(e) => checkWords(e.target.value)} /> 's one thing I know is true</span>
  <span>When nocturnal notions are construed</span>
  <span>Be it a blessing or a <StyledInput onChange={(e) => checkWords(e.target.value)} /> </span>
  <span>I'm here for better, or for <StyledInput onChange={(e) => checkWords(e.target.value)} /> </span>
  <br/>
  <br/>  
  <span>I'll sing you <StyledInput onChange={(e) => checkWords(e.target.value)} />  sleep tonight</span>
  <span>Make sure everything's alright</span>
  <span>No more worries, doubts, I'm <StyledInput onChange={(e) => checkWords(e.target.value)} />  out</span>
  <span>I'll keep you safe until we see the light</span>
  <br/>
  <br/>
  <span>I know things may seem a mess, but I will never quit</span>
  <span>You're my <StyledInput onChange={(e) => checkWords(e.target.value)} />  side, eyes wide open every night</span>
  <span>I'm by your side, I'll never ask <StyledInput onChange={(e) => checkWords(e.target.value)} /> </span>
  <span>Here's your <StyledInput onChange={(e) => checkWords(e.target.value)} /> </span>
  <br/>
  <br/>

  <h5>This is my bank: {myBank}</h5>
  <h5>Total words I could figure out is: {score}</h5>
  {score === 9 && <div> Congratulations! You've achieved the maximum score!</div>}
</Container>
</motion.div>
);

}


export default NocturnalNotions;