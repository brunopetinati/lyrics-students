import styled from "styled-components"

export const Container = styled.div`
background-color: #dcdeed;

display:flex;
flex-direction: column;
justify-content: space-evenly;
align-items: center;

width:100vw;
height: 100%;

padding-bottom: 10%;

font-family: Arial, Helvetica, sans-serif;
font-size: 20px;
letter-spacing: 1px;
word-spacing: 1px;
color: #000000;
`;

export const StyledInput = styled.input`
  font-size: 18px;
  padding: 10px;
  margin: 10px;
  background: #edf0ff;
  width:7vw;
  height:1vh;
  border: none;
  outline: none;
  color:#353539;
  text-align: center; 
  border-radius: 3px;
  ::placeholder {
    color: #353539;
  }
`;

export const HomeContainer = styled.div`
  background-color:black;
  color:white;
  display:flex;
  flex-direction:row;
  align-items: center;
  justify-content: space-evenly;
  height: 100vh;
  width:100vw;
`;


export const Links = styled.button`
  background-color: #0095ff;
  border: 1px solid transparent;
  border-radius: 3px;
  box-shadow: rgba(255, 255, 255, .4) 0 1px 0 0 inset;
  box-sizing: border-box;
  color: #fff;
  cursor: pointer;
  display: inline-block;
  font-family: -apple-system,system-ui,"Segoe UI","Liberation Sans",sans-serif;
  font-size: 13px;
  font-weight: 400;
  line-height: 1.15385;
  margin: 0;
  outline: none;
  padding: 8px .8em;
  position: relative;
  text-align: center;
  text-decoration: none;
  user-select: none;
  -webkit-user-select: none;
  touch-action: manipulation;
  vertical-align: baseline;
  white-space: nowrap;
}

&:hover {
  background-color: #07c;
}

&:focus {
  box-shadow: 0 0 0 4px rgba(0, 149, 255, .15);
}

&:active {
  background-color: #0064bd;
  box-shadow: none;
}
`;

export const Space = styled.div`
height: 70px;
`;

