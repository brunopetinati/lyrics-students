import {  Switch, Route } from "react-router-dom";
import { AnimatePresence } from "framer-motion";


import Home from "../pages/Home/index"
import Renegades from "../pages/X-Ambassors/index"
import RiptideVanceJoy from "../pages/Vance Joy/index"
import StayTheKidLaroi from "../pages/The Kid Laroi/index"
import NocturnalNotions from  "../pages/Anthony Amorim/index"
import WonderWallRyanAdams from "../pages/Ryan Adans - Wonderwall/index"
import NextToMeRufusDuSol from "../pages/Rufus du Sol - Next to me/index"

const Routes = () => {
    
  
    return (
      <AnimatePresence>
        <Switch>
            <Route exact path="/">
                <Home />
            </Route>
            <Route exact path="/renegades">
                <Renegades/> 
            </Route>
            <Route exact path="/riptide">
            <RiptideVanceJoy/>
            </Route>
            <Route exact path="/stay-the-kid-laroi">
                <StayTheKidLaroi />
            </Route>
            <Route exact path="/nocturnalnotions">
            <NocturnalNotions />
            </Route>
            <Route exact path="/wonderwall">
            <WonderWallRyanAdams />
            </Route>
            <Route exact path="/nextome">
            <NextToMeRufusDuSol />
            </Route>
        </Switch>
      </AnimatePresence>
    );
  };
  
  export default Routes;